import requests

api_key = open('api_key.txt', 'r').read()
urls = requests.get('https://api.semanticscholar.org/datasets/v1/release/latest/dataset/s2orc', headers={'x-api-key': api_key.rstrip()}).json()['files']

with open('urls.txt', 'w') as f:
    for url in urls:
        f.write("%s\n" % url)
