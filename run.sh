#!/bin/bash

python3 get_urls.py

mkdir -p resource

counter=0

while read url; do
    filename="s2orc_${counter}.gz"
    curl -o "resource/${filename}" "$url"
    ((counter++))
done < urls.txt
